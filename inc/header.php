<!DOCTYPE HTML>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>CRUD with php mysqli</title>
  <style>
body {
  font-family: arial;
  font-size: 18px;
  line-height: 22px;
  margin: 0 auto;
  width: 800px;
}
.phpcoding {
  width:900px; margin: 0px auto; background-color: #ddd;
}
.headeroption {
  background: #3399ff url("img/php.png") no-repeat scroll 56px 18px;
  height: 80px;
  overflow: hidden;
  padding-left: 160px;
}
.footeroption{height:80px;background:#3399FF;overflow:hidden;}
.headeroption h2 {
  color: #000;
  font-size: 30px;
  padding-top: 5px;
  text-shadow: 0 1px 1px #fff;
}
.footeroption h2 {
  background: rgba(0, 0, 0, 0) url("img/logo.png") no-repeat scroll 65px 0;
  color: #000;
  font-size: 30px;
  padding-bottom: 13px;
  padding-top: 10px;
  text-align: center;
  text-shadow: 1px 1px 1px #fff;
}
.content {
  background: #f2f2ff none repeat scroll 0 0;
  border: 6px solid #3399ff;
  font-size: 16px;
  line-height: 22px;
  margin-bottom: 8px;
  margin-top: 8px;
  min-height: 420px;
  overflow: hidden;
  padding: 10px;
}
.tblone {
  width: 100%; border: 1px solid #fff; margin: 20px 0px;
}
.tblone td {
  padding: 5px 10px; text-align: center;
}
table.tblone tr:nth-child(2n+1) {
  background-color: #fff; height: 30px;
}
table.tblone tr:nth-child(2n) {
  background-color: #f1f1f1; height: 30px;
}
h2 {
  margin-left: 80px;
}
  </style>
</head>
<body>
  <header class="headeroption">
    <h2>CRUD with php mysqli</h2>
  </header>
  
  <section class="content">