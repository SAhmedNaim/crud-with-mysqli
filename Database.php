<?php
	
	class Database {
		public $host	= DB_HOST;
		public $user 	= DB_USER;
		public $pass 	= DB_PASS;
		public $dbname 	= DB_NAME;

		public $link;
		public $error;

		// default constructor
		public function __construct() {
			$this->connectDB();
		}

		// connection method
		private function connectDB() {
			$this->link = new mysqli($this->host, $this->user, $this->pass, $this->dbname);
			if (!$this->link) {
				$this->error = "Connection Failed With Database ".$this->link->connect_error;		
				return false;
			} // if condition finish
		} // connectDB() finish

		// Read Data from database
		public function select($query) {
			$result = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($result->num_rows > 0) {
				return $result;
			} else {
				return false;
			}
			
		}

		// create data to database
		public function insert($query) {
			$insert_row = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($insert_row) {
				header("Location: index.php?msg=".urlencode('Data Inserted successfully!'));	
				exit();
			} else {
				die("Error! (".$this->link->errno.")".$this->link->error);
			}
		}

		// update data to database
		public function update($query) {
			$update_row = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($update_row) {
				header("Location: index.php?msg=".urlencode('Data Updated successfully!'));	
				exit();
			} else {
				die("Error! (".$this->link->errno.")".$this->link->error);
			}
		}

		// delete data from database
		public function delete($query) {
			$delete_row = $this->link->query($query) or die($this->link->error.__LINE__);
			if ($delete_row) {
				header("Location: index.php?msg=".urlencode('Data Deleted successfully!'));	
				exit();
			} else {
				die("Error! (".$this->link->errno.")".$this->link->error);
			}
		}


	} // class Database finish


?>